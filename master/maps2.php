<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <script type="text/javascript"
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnQ6AsVyQMzyNDf-FVo1Suia5nQqw6H3c">
    </script> 
</head>
<body>

<div class="container">
  <h2>GET LATITUDE, LONGITUDE</h2>
  <div class="col-xs-4">
  <input type="text" name="" class="form-control" id="latspan" >
  <p id="lngspan" style="display: none"></p>
  </div>
  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Koordinat</button>
</div>

<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Google Maps</h4>
                </div>
                <div class="modal-body">

                    <div id="map_canvas" style="width:auto; height: 500px;"></div>
                </div>
                <div class="modal-footer">
                     <button type="button" class="btn btn-primary" data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
	var marker;
	function taruhMarker(peta, posisiTitik){
	    if( marker ){
	      // pindahkan marker
	      marker.setPosition(posisiTitik);
	    } else {
	      // buat marker baru
	      marker = new google.maps.Marker({
	        position: posisiTitik,
	        map: peta
	      });
	    }
	    
	}
    function initializeMap() {
    	var tmp_lat = -6.8268786;
      	var tmp_lng = 111.046347;
        var mapOptions = {
            center: new google.maps.LatLng(tmp_lat,tmp_lng),
             zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var peta = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  
	  // even listner ketika peta diklik
	  google.maps.event.addListener(peta, 'click', function(event) {
	    taruhMarker(this, event.latLng);
	    document.getElementById("latspan").value = event.latLng.lat();
     	document.getElementById("lngspan").value = event.latLng.lng();
     	var a = [event.latLng.lat(),event.latLng.lng()];
     	var variabel2 = a.join(', ');
     	document.getElementById("latspan").value = variabel2;
	  });
    }


    //show map on modal
    $('#myModal').on('shown.bs.modal', function () {
        initializeMap();
    });

</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
