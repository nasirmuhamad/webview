 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAeSX_ZkrWyZ1X6GWHkFIjd77iwydLZr1o"

sensor=false"></script>
<script type="text/javascript">
  var directionsDisplay;
  var directionsService = new google.maps.DirectionsService();
  var map;
  var oldDirections = [];
  var currentDirections = null;

  function initialize() {
    var myOptions = {
      zoom: 12,
      center: new google.maps.LatLng(-7.805155310233364,110.36424198305053),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas3"), 

myOptions);

    directionsDisplay = new google.maps.DirectionsRenderer({
        'map': map,
        'preserveViewport': true,
        'draggable': true
    });
    directionsDisplay.setPanel(document.getElementById("directions_panel"));

    google.maps.event.addListener(directionsDisplay, 'directions_changed',
      function() {
        if (currentDirections) {
          oldDirections.push(currentDirections);
          setUndoDisabled(false);
        }
        currentDirections = directionsDisplay.getDirections();
      });

    setUndoDisabled(true);

    calcRoute();
  }

  function calcRoute() {
    var start = 'Kraton Yogyakarta, Yogyakarta';
    var end = '-7.782726399860902,110.40125352935797';
    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(response);
      }
    });
  }

  function undo() {
    currentDirections = null;
    directionsDisplay.setDirections(oldDirections.pop());
    if (!oldDirections.length) {
      setUndoDisabled(true);
    }
  }

  function setUndoDisabled(value) {
    document.getElementById("undo").disabled = value;
  }

  google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map_canvas3" style="float:left;width:300px;height:300px"></div>
<div style="float:right;width:30%;height:100%;overflow:auto">
  <button id="undo" style="display:block;margin:auto" onclick="undo()">Undo
  </button>
  <div id="directions_panel" style="width:200"></div>
</div>