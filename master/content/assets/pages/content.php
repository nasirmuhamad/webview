<!-- Header -->
<?php $this->load->view('header'); ?>

<!-- Top Navigation -->
<?php $this->load->view('topbar'); ?>

<!-- Star Content -->
<section id="main">

	<?php $this->load->view('sidebar'); ?>
	<?php $this->load->view('chatbar'); ?>
	
	<section id="content">
		<div class="container">
			<?php $this->load->view($page); ?>
		</div>
	</section>
	
</section>
<!-- End Content -->

<!-- Footer -->
<?php $this->load->view('footer'); ?>
