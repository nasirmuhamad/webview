<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Restoran</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Masukan data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Gambar restoran</label><br>
                                            <img width="200" id="preview" height="auto" src="" />
                                            <input style="padding-top: 10px" type="file" name="gambar" onchange="tampilkanPreview(this,'preview')">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama restoran</label>
                                            <input class="form-control" name="nama">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Lokasi</label>
                                            <input class="form-control" name="lokasi">
                                        </div>
                                        <div class="form-group">
                                            <label>Waktu buka</label>
                                            <input class="form-control" name="waktu_buka">
                                        </div>
                                         <div class="form-group">
                                            <label>Instagram</label>
                                            <input class="form-control" name="ig">
                                        </div>
                                         <div class="form-group">
                                            <label>Telepon</label>
                                            <input class="form-control" name="tlp">
                                        </div>
                                         <div class="form-group">
                                            <label>Maps</label>
                                            <input class="form-control" name="maps">
                                        </div>
                                        <button type="Submit" class="btn btn-default" name="simpan">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div> 
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script> 
<?php
error_reporting(0); 
if (isset($_POST['simpan']))
{
    include('koneksi.php');
    $lokasi_file = $_FILES['gambar']['tmp_name'];
    $tipe_file   = $_FILES['gambar']['type'];
    $nama_file   = $_FILES['gambar']['name'];
    $direktori   = "../images/gambar_restoran/$nama_file";

    $a = $_POST[nama];
    $b = $_POST[lokasi];
    $c = $_POST[waktu_buka];

        if ($a != null  AND $b != null AND $c!=null AND !empty($lokasi_file)) {
             include('koneksi.php');
            move_uploaded_file($lokasi_file,$direktori);
            $ba =  mysqli_query($koneksi, "INSERT INTO restoran(gambar_restoran,nama_restoran,lokasi,waktu,instagram,telepon,maps)VALUES('$nama_file','$_POST[nama]','$_POST[lokasi]','$_POST[waktu_buka]','$_POST[ig]','$_POST[tlp]','$_POST[maps]')")or die(mysqli_error());
             echo "<script>
                swal('Berhasil!', 'Data restoran berhasil ditambahkan', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=restoran'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Lengkapi Data Dahulu!', 'error')
              </script>";
        }
} ?>
<script>
    function tampilkanPreview(gambar,idpreview){
//                membuat objek gambar
        var gb = gambar.files;
        
//                loop untuk merender gambar
        for (var i = 0; i < gb.length; i++){
//                    bikin variabel
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview=document.getElementById(idpreview);            
            var reader = new FileReader();
            
            if (gbPreview.type.match(imageType)) {
//                        jika tipe data sesuai
                preview.file = gbPreview;
                reader.onload = (function(element) { 
                    return function(e) { 
                        element.src = e.target.result; 
                    }; 
                })(preview);
                document.getElementById("preview").style.border = "1px solid black"; 

//                    membaca data URL gambar
                reader.readAsDataURL(gbPreview);
            }else{
//                        jika tipe data tidak sesuai
                alert("Type file tidak sesuai. Khusus image.");
            }
           
        }    
    }
</script>