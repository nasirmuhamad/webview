<?php 
include('koneksi.php');
$ambil=mysqli_query($koneksi, "SELECT * FROM kategori_makanan WHERE id_kategori='$_GET[id]'");
$bagi =$ambil->fetch_assoc();

?>

 <!-- Main content -->
 	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Kategori makanan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit kategori
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                        	<label>gambar kategori :</label><br>
  											<img style="border:1px solid black;" width="200" id="preview" height="auto" id="blah" src="../images/gambar_kategori/<?php echo $bagi['gambar_kategori']?>" />
  											<input style="padding-top: 10px" type="file" accept="image/*" name="gambar"  onchange="tampilkanPreview(this,'preview')" />    										
                                        </div>
                                        <div class="form-group">
                                            <label>Nama kategori :</label>
                                             <input class="form-control" name="nama" value="<?php echo $bagi['nama_kategori'];?>" required>
                                            
                                        </div>
                                        <button type="Submit" class="btn btn-default" name="simpan">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script> 
<?php 
if (isset($_POST['simpan']))
{

    include('koneksi.php');
    $lokasi_file = $_FILES['gambar']['tmp_name'];
    $tipe_file   = $_FILES['gambar']['type'];
    $nama_file   = $_FILES['gambar']['name'];
    $direktori   = "./images/gambar_kategori/$nama_file";

    $namanya = $_POST['nama'];

    if ($nama_file == "") {
    	$a =  mysqli_query($koneksi, "UPDATE kategori_makanan SET nama_kategori='$_POST[nama]' WHERE id_kategori='$_GET[id]' ");
        if ($a) {
             echo "<script>
                swal('Berhasil!', 'Kategori Berhasil Ditambahkan!', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=kategori'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Kategori Gagal Ditambahkan!', 'error')
              </script>";
        }
    }else{
    move_uploaded_file($lokasi_file,$direktori);
    $a =  mysqli_query($koneksi, "UPDATE kategori_makanan SET nama_kategori='$_POST[nama]',gambar_kategori='$nama_file' WHERE id_kategori='$_GET[id]' ");
        if ($a) {
             echo "<script>
                swal('Berhasil!', 'Kategori Berhasil Ditambahkan!', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=kategori'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Kategori Gagal Ditambahkan!', 'error')
              </script>";
        }
   	}
} ?>


<script>
    function tampilkanPreview(gambar,idpreview){
//                membuat objek gambar
        var gb = gambar.files;
        
//                loop untuk merender gambar
        for (var i = 0; i < gb.length; i++){
//                    bikin variabel
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview=document.getElementById(idpreview);            
            var reader = new FileReader();
            
            if (gbPreview.type.match(imageType)) {
//                        jika tipe data sesuai
                preview.file = gbPreview;
                reader.onload = (function(element) { 
                    return function(e) { 
                        element.src = e.target.result; 
                    }; 
                })(preview);
                document.getElementById("preview").style.border = "1px solid black";

//                    membaca data URL gambar
                reader.readAsDataURL(gbPreview);
            }else{
//                        jika tipe data tidak sesuai
                alert("Type file tidak sesuai. Khusus image.");
            }
           
        }    
    }
</script>