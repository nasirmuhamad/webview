<?php 
include('koneksi.php');
$ambil=mysqli_query($koneksi, "SELECT * FROM menu WHERE id_menu='$_GET[id]'");
$bagi =$ambil->fetch_assoc();

?>

 <!-- Main content -->
 	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menu makanan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit menu
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Nama restoran :</label>
                                                <select class="form-control" name="restoran" required>
                                                <?php
                                                include('koneksi.php');
                                                $sql = mysqli_query($koneksi,"SELECT * FROM restoran");
                                                if(mysqli_num_rows($sql) != 0){
                                                    while($data = mysqli_fetch_assoc($sql)){
                                                        if($bagi['id_restoran'] == $data['id_restoran']){
                                                            echo "<option value='".$bagi["id_restoran"]."' selected='selected'>".$data["nama_restoran"]."</option>"; 
                                                        }else{ 
                                                            echo "<option value='".$data["id_restoran"]."'>".$data["nama_restoran"]."</option>"; 
                                                        }
                                                    }
                                                }
                                                ?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama kategori :</label>
                                                <select class="form-control" name="kategori" required>
                                                <?php
                                                include('koneksi.php');
                                                $sql = mysqli_query($koneksi,"SELECT * FROM kategori_makanan");
                                                if(mysqli_num_rows($sql) != 0){
                                                    while($data = mysqli_fetch_assoc($sql)){
                                                        if($bagi['id_kategori'] == $data['id_kategori']){
                                                            echo "<option value='".$bagi["id_kategori"]."' selected='selected'>".$data["nama_kategori"]."</option>"; 
                                                        }else{ 
                                                            echo "<option value='".$data["id_kategori"]."'>".$data["nama_kategori"]."</option>"; 
                                                        }
                                                    }
                                                }
                                                ?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama menu :</label>
                                            <input type="text" class="form-control" name="nama" value="<?php echo $bagi['nama_menu'];?>" required>
                                            
                                        </div>
                                        <button type="Submit" class="btn btn-default" name="simpan">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script> 
<?php

if (isset($_POST['simpan']))
{

        include('koneksi.php');
    	$a =  mysqli_query($koneksi, "UPDATE menu SET id_restoran='$_POST[restoran]', nama_menu='$_POST[nama]', id_kategori='$_POST[kategori]' WHERE id_menu='$_GET[id]' ");
        if ($a) {
             echo "<script>
                swal('Berhasil!', 'Kategori Berhasil Ditambahkan!', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=menu'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Kategori Gagal Ditambahkan!', 'error')
              </script>";
        }
  
} ?>