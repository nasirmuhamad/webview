<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Restoran</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
              <a href="index.php?halaman=tambah_restoran" class="btn btn-success">Tambah restoran</a>

            <!-- /.row -->
            <br><br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Daftar restoran
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">No</th>
                                        <th style="width: 5%">Gambar restoran</th>
                                        <th style="width: 10%">Nama restoran</th>
                                        <th style="width: 15%">lokasi</th>
                                        <th style="width: 10%">waktu</th>
                                        <th style="width: 10%">Instagram</th>
                                        <th style="width: 10%">Telepon</th>
                                        <th style="width: 10%">Maps</th>

                                       
                                        <th style="text-align: center;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php include('koneksi.php'); ?>
                                    <?php $restoran = $koneksi->query("SELECT * from restoran"); ?>
                                    <?php $no = 1; while($a = $restoran->fetch_assoc()){?>
                                    <tr class="odd gradeX">
                                        <td style="text-align: center;"><?php echo $no++; ?></td>
                                        <td><img width="100" id="preview" height="auto" id="blah" src="../images/gambar_restoran/<?php echo $a['gambar_restoran']?>" /></td>
                                        <td><?php echo $a['nama_restoran'] ?></td>
                                        <td><?php echo $a['lokasi'] ?></td>
                                        <td><?php echo $a['waktu'] ?></td>
                                        <td><?php echo $a['instagram'] ?></td>
                                        <td><?php echo $a['telepon'] ?></td>
                                        <td><?php echo $a['maps'] ?></td>
                                        <td style="text-align: center;">
                                             <a href='index.php?halaman=edit_restoran&id=<?= $a['id_restoran']?>'
                                             class="btn btn-primary">Edit</a>
                                             <button onclick="confirmDelete(<?= $a['id_restoran']?>)" class="btn btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script>  
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  
<script src="js/jquery-1.7.1.min.js"></script> 

<script type="text/javascript">
    var hapus = "hapus_restoran"; 
    function confirmDelete($id) {
       swal({
          title: "Apakah anda yakin ingin menghapus ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
       .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                url: "../content/fungsi_hapus.php",
                type: "POST",
                data: {
                    id: $id,
                    hapus : hapus
                },
                dataType: "html",
                success: function () {
                    swal("Terhapus","Data berhasil dihapus!","success");
                    window.setTimeout(function(){window.location.reload()}, 2000);
                }
            });
          } else {
            swal("Data tidak jadi terhapus");
          }
        });
    }
</script>