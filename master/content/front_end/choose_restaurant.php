<!DOCTYPE html> 
<html> 
<head> 
	<meta charset="UTF-8">
	<title>Restaurant Picker</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="../jquery.mobile.structure-1.0.1.css" />
	<link rel="apple-touch-icon" href="../images/launch_icon_57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="../images/launch_icon_72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="../images/launch_icon_114.png" />
	<link rel="stylesheet" href="../jquery.mobile-1.0.1.css" />
	<link rel="stylesheet" href="../custom.css" />
	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery.mobile-1.0.1.min.js"></script>
</head> 
<body> 
<div id="choisir_ville" data-role="page" data-add-back-btn="true">
	
	<a href="index.php?halaman1=front"><div id="branding">
		<h1>Restaurant Picker </h1>
	</div></a>

	<div data-role="content">
	
	<div class="choice_list"> 
	<h1> Pilih restoran yang diinginkan</h1>
	
	<ul data-role="listview" data-inset="true" data-filter="true" >
	<?php include('koneksi.php'); ?>
	<?php $kategori = $koneksi->query("SELECT a.* from restoran a join menu b on a.id_restoran=b.id_restoran join kategori_makanan c on c.id_kategori=b.id_kategori where c.id_kategori='$_GET[id]'"); ?>
	<?php while($a = $kategori->fetch_assoc()){?>
	<li><a href="index.php?halaman1=restoran_detail&id=<?php echo $a['id_restoran'];?>" data-transition="slidedown" style="height: 59px"> <img style="height: 95px;width: 200px" src="../images/gambar_restoran/<?php echo $a['gambar_restoran']?>"/> <h3> <?php echo $a['nama_restoran'] ?></h3></a></li> <!-- Terserah</h2></a></li>
	<li><a href="restaurant.html" data-transition="slidedown"> <img src="restau02_mini.jpg "/> <h2> Pizza  </h2> <p class="classement four"> 4 stars  </p> </a></li>
	<li><a href="restaurant.html" data-transition="slidedown"> <img src="restau03_mini.jpg "/> <h2> Restaurant Diona</h2> <p class="classement one"> 1 star  </p> </a></li>
	<li><a href="restaurant.html" data-transition="slidedown"> <img src="restau04_mini.jpg "/> <h2> Tai Shan</h2> <p class="classement three"> 3 stars  </p> </a></li>
	<li><a href="restaurant.html" data-transition="slidedown"> <img src=" restau05_mini.jpg"/> <h2> Arcade</h2> <p class="classement two"> 2 stars </p> </a></li> -->
	<?php }?>
	</ul>	
	
	</div>
	</div>

</div><!-- /page -->
</body>
</html>