<!DOCTYPE html> 
<html> 
<head> 
	<meta charset="UTF-8">
	<title>Restaurant Picker</title> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="../jquery.mobile.structure-1.0.1.css" />
	<link rel="apple-touch-icon" href="../images/launch_icon_57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="../images/launch_icon_72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="../images/launch_icon_114.png" />
	<link rel="stylesheet" href="../jquery.mobile-1.0.1.css" />
	<link rel="stylesheet" href="../custom.css" />
	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery.mobile-1.0.1.min.js"></script>
</head> 
<body> 
<div id="resto1" data-role="page" data-add-back-btn="true">
	
	<div data-role="header"> 
		<h1> Restaurant Picker</h1>
	</div> 
	<?php include('koneksi.php'); ?>
	<?php $restoran = $koneksi->query("SELECT * from restoran where id_restoran='$_GET[id]'"); 
	$a = $restoran->fetch_assoc()?>
	<div data-role="content">
	<div class="ui-grid-a" id="restau_infos">	
		<div class="ui-block-a">
		<h1><?php echo $a['nama_restoran'];?></h1>
		<br/>
		<p>menu kami: </p>
			
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<p>Jam buka :&nbsp<?php echo $a['waktu'];?></p>	
		<p>Tlp.<?php echo $a['telepon'];?></p>
		<p><?php echo $a['lokasi'];?></p>	
		</div>		
		<div class="ui-block-b">
		<p><img src="../images/gambar_restoran/<?php echo $a['gambar_restoran'];?>"></p>
		<p><a href="http://www.google.fr" rel="external" data-role="button"> See our Instagram</a></p>
		</div>
	</div><!-- /grid-a -->
	<hr/>
	
	<div class="ui-grid-a" id="contact_infos">	
		<div class="ui-block-a">
		<!-- <h2> Contact us</h2>
		<p><?php echo $a['lokasi'];?></p>		
		</div>		
		<div class="ui-block-b">
		<img src="01_maps.jpg" alt="plan jeanette"/> -->
			<img src=../sukoharjo.jpg width="700" height="300" frameborder="0" style="border:0" allowfullscreen></img>
		</div>
	</div><!-- /grid-a -->
	<div id="contact_buttons">	
		<a href="https://www.google.com/maps/place/Jl.+Jend.+Sudirman,+Kabupaten+Sukoharjo,+Jawa+Tengah/@-7.6525892,110.8308772,17z/data=!3m1!4b1!4m5!3m4!1s0x2e7a3c4987ce357f:0x3f86de4787baa262!8m2!3d-7.6525892!4d110.8330659" data-role="button" data-icon="maps" onclick="getLocation()"> Find us on Google Maps </a> 	
	</div>	
	<hr/>
 <div id="map"></div>
 <script type="text/javascript">
 	function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude;
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}
</script>

    <script>

      var map, infoWindow;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 6
        });
        infoWindow = new google.maps.InfoWindow;

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Location found.');
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
      }

      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
    </script>


	

	</div>


</div><!-- /page -->
<!-- /page -->

</body>
</html>