<!DOCTYPE html> 
<html> 
	<head>
  <meta charset="UTF-8">	
	<title>Restaurant Picker</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<link rel="stylesheet" href="../jquery.mobile.structure-1.0.1.css" />
	<link rel="apple-touch-icon" href="../images/launch_icon_57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="../images/launch_icon_72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="../images/launch_icon_114.png" />
	<link rel="stylesheet" href="../jquery.mobile-1.0.1.css" />
	<link rel="stylesheet" href="../custom.css" />
	<script src="../js/jquery-1.7.1.min.js"></script>
	<script src="../js/jquery.mobile-1.0.1.min.js"></script>
</head> 

<body> 
<div data-role="page" id="home" data-theme="c">

	<div data-role="content">
	
	<div id="branding">
		<h1>Restaurant Picker </h1>
	</div>
	
	<div class="choice_list"> 
	<h1> Mau makan apa hari ini ? </h1>
	
	<ul data-role="listview" data-inset="true" data-filter="true" data-input="#filter-kategori" >
	<?php include('koneksi.php'); ?>
	<?php $kategori = $koneksi->query("SELECT * from kategori_makanan"); ?>
	<?php while($a = $kategori->fetch_assoc()){?>
	<li><a href='index.php?halaman1=restaurant_front&id=<?= $a['id_kategori']?>'  data-transition="slidedown" style="height: 59px"> <img style="height: 95px;width: 200px" src="../images/gambar_kategori/<?php echo $a['gambar_kategori']?>"/> <h3> <?php echo $a['nama_kategori'] ?></h3></a></li>
	<!-- <li><a href="choose_town.html"  data-transition="slidedown"> <img src="pizza.jpg"/> <h3> A Pizza </h3></a></li> -->
	<!-- <li><a href="choose_town.html"  data-transition="slidedown"> <img src="kebap.jpg"/> <h3> A Kebap</h3></a></li> -->
	<!-- <li><a href="choose_town.html"  data-transition="slidedown"> <img src="burger.jpg"/> <h3> A Burger</h3></a></li> -->
	<!-- <li><a href="choose_town.html"  data-transition="slidedown"> <img src="nems.jpg"/> <h3> Some Nems </h3></a></li> -->
	<!-- <li><a href="choose_town.html"  data-transition="slidedown"> <img src="tradi.jpg"/> <h3> Something more traditional</h3></a></li>	 -->
	<?php } ?>
	</ul>	
	
	</div>
	</div>

</div><!-- /page -->
</body>
</html>
