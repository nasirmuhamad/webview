<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menu makanan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
             <a href="index.php?halaman=tambah_menu" class="btn btn-success">Tambah menu</a>
            <!-- /.row -->
            <br><br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Daftar Kategori
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;">No</th>
                                        <th style="width: 30%">nama restoran</th>
                                        <th style="width: 20%">Nama Kategori</th>
                                        <th style="width: 20%">Nama menu</th>
                                        <th style="text-align: center;">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php include('koneksi.php'); ?>
                                    <?php $menu = $koneksi->query("SELECT a.id_menu, a.nama_menu, b.nama_kategori, c.nama_restoran from menu a join kategori_makanan b on a.id_kategori=b.id_kategori join restoran c on a.id_restoran=c.id_restoran ");
                                    // $menu2 = $koneksi->query("SELECT menu.id_menu, menu.nama_menu, kategori_makanan.nama_kategori, restoran.nama_restoran from menu, kategori_makanan, restoran where menu.id_kategori=kategori_makanan.id_kategori and menu.id_restoran=restoran.id_restoran");    
                                     ?>
                                    <?php $no = 1; while($a = $menu->fetch_assoc()){?>
                                    <tr class="odd gradeX">
                                        <td style="text-align: center;"><?php echo $no++; ?></td>
                                        <td> <?= $a['nama_restoran'] ?> </td>
                                        <td> <?= $a['nama_kategori'] ?> </td>
                                        <td> <?= $a['nama_menu'] ?> </td>
                                        <td style="text-align: center;">
                                             <a href='index.php?halaman=edit_menu&id=<?= $a['id_menu']?>' class="btn btn-primary">Edit</a>
                                            <button onclick="confirmDelete(<?= $a['id_menu']?>)" class="btn btn-danger">Delete</button>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                             <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->
       
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script>  
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  
<script src="js/jquery-1.7.1.min.js"></script> 

<script type="text/javascript">
    var hapus ="hapus_menu";
    function confirmDelete($id) {
       swal({
          title: "Apakah anda yakin ingin menghapus ini?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
       .then((willDelete) => {
          if (willDelete) {
            $.ajax({
                url: "../content/fungsi_hapus.php",
                type: "POST",
                data: {
                    id: $id,
                    hapus : hapus,
                },
                dataType: "html",
                success: function () {
                    swal("Terhapus","Data berhasil dihapus!","success");
                    window.setTimeout(function(){window.location.reload()}, 2000);
                }
            });
          } else {
            swal("Data tidak jadi terhapus");
          }
        });
    }
</script>