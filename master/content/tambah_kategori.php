<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Kategori makanan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Masukan data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Gambar Kategori</label><br>
                                            <img width="200" id="preview" height="auto" src="" />
                                            <input style="padding-top: 10px" type="file" name="gambar" onchange="tampilkanPreview(this,'preview')">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama kategori</label>
                                            <input class="form-control" name="nama">
                                            
                                        </div>
                                        <button type="Submit" class="btn btn-default" name="simpan">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script> 
<?php 
error_reporting(0); 
if (isset($_POST['simpan']))
{
    include('koneksi.php');
    $lokasi_file = $_FILES['gambar']['tmp_name'];
    $tipe_file   = $_FILES['gambar']['type'];
    $nama_file   = $_FILES['gambar']['name'];
    $direktori   = "../images/gambar_kategori/$nama_file";

    $namanya = $_POST['nama'];
    
    if (!empty($lokasi_file) AND !empty($namanya) ){
    move_uploaded_file($lokasi_file,$direktori);
    $a =  mysqli_query($koneksi, "INSERT INTO kategori_makanan(nama_kategori, gambar_kategori)VALUES('$_POST[nama]', '$nama_file')")or die(mysqli_error());
        if ($a) {
             echo "<script>
                swal('Berhasil!', 'Data kategori berhasil ditambahkan!', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=kategori'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Lengkapi data dahulu!', 'error')
              </script>";
        }
    }else{
        echo "<script>
    swal('Gagal!', 'Lengkapi data!', 'error')
  </script>"; 
  }
} ?>

<script>
    function tampilkanPreview(gambar,idpreview){
//                membuat objek gambar
        var gb = gambar.files;
        
//                loop untuk merender gambar
        for (var i = 0; i < gb.length; i++){
//                    bikin variabel
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview=document.getElementById(idpreview);            
            var reader = new FileReader();
            
            if (gbPreview.type.match(imageType)) {
//                        jika tipe data sesuai
                preview.file = gbPreview;
                reader.onload = (function(element) { 
                    return function(e) { 
                        element.src = e.target.result; 
                    }; 
                })(preview);
                document.getElementById("preview").style.border = "1px solid black"; 

//                    membaca data URL gambar
                reader.readAsDataURL(gbPreview);
            }else{
//                        jika tipe data tidak sesuai
                alert("Type file tidak sesuai. Khusus image.");
            }
           
        }    
    }
</script>