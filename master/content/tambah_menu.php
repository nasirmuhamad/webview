<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Menu makanan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Masukan data
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Nama restoran :</label>
                                            <select class="form-control" name="nama_restoran">
                                            <option selected disabled>pilih salah satu !</option>
                                            <?php
                                            include('koneksi.php');
                                            $sql = mysqli_query($koneksi,"SELECT * FROM restoran ORDER BY id_restoran ASC");
                                            if(mysqli_num_rows($sql) != 0){
                                                while($data = mysqli_fetch_assoc($sql)){
                                                    echo '<option name="" value='.$data['id_restoran'].'>'.$data['nama_restoran'].'</option>';
                                                }
                                            }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama kategori :</label>
                                            <select class="form-control" name="nama_kategori">
                                            <option selected disabled>pilih salah satu !</option>
                                            <?php
                                            include('koneksi.php');
                                            $sql = mysqli_query($koneksi,"SELECT * FROM kategori_makanan ORDER BY id_kategori ASC");
                                            if(mysqli_num_rows($sql) != 0){
                                                while($data = mysqli_fetch_assoc($sql)){
                                                    echo '<option name="" value='.$data['id_kategori'].'>'.$data['nama_kategori'].'</option>';
                                                }
                                            }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama menu :</label>
                                            <input type="text" class="form-control" name="nama_menu">
                                        </div>
                                        <button type="Submit" class="btn btn-default" name="simpan">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script> 
<?php
error_reporting(0); 
if (isset($_POST['simpan']))
{
    $a =$_POST[nama_restoran];
    $b = $_POST[nama_kategori];
    $c = $_POST[nama_menu];
    
        if ($a != null  AND $b != null AND $c!=null) {
            include('koneksi.php');
             $ba =  mysqli_query($koneksi, "INSERT INTO menu(id_restoran,id_kategori,nama_menu)VALUES('$_POST[nama_restoran]','$_POST[nama_kategori]','$_POST[nama_menu]')")or die(mysqli_error());
             echo "<script>
                swal('Berhasil!', 'Data menu berhasil ditambahkan!', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=menu'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Lengkapi data dahulu!', 'error')
              </script>";
        }
} ?>