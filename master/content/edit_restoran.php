<?php 
include('koneksi.php');
$ambil=mysqli_query($koneksi, "SELECT * FROM restoran WHERE id_restoran='$_GET[id]'");
$bagi =$ambil->fetch_assoc();

?>

 <!-- Main content -->
 	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar restoran</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit restoran
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>gambar kategori :</label><br>
                                            <img style="border:1px solid black;" width="200" id="preview" height="auto" id="blah" src="../images/gambar_restoran/<?php echo $bagi['gambar_restoran']?>" />
                                            <input style="padding-top: 10px" type="file" accept="image/*" name="gambar"  onchange="tampilkanPreview(this,'preview')" />                                         
                                        </div>
                                        <div class="form-group">
                                            <label>Nama restoran :</label>
                                            <input class="form-control" name="nama" value="<?php echo $bagi['nama_restoran'];?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Lokasi :</label>
                                            <input class="form-control" name="lokasi" value="<?php echo $bagi['lokasi'];?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Waktu buka :</label>
                                            <input class="form-control" name="waktu" value="<?php echo $bagi['waktu'];?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Instagram</label>
                                            <input class="form-control" name="instagram" value="<?php echo $bagi['instagram'];?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Telepon</label>
                                            <input class="form-control" name="tlp" value="<?php echo $bagi['telepon'];?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>maps</label>
                                            <input class="form-control" name="maps" value="<?php echo $bagi['maps'];?>" required>
                                        </div>
                                        <button type="Submit" class="btn btn-default" name="simpan">Submit Button</button>
                                        <button type="reset" class="btn btn-default">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
<link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="sweetalert/dist/sweetalert.min.js"></script> 
<?php 
if (isset($_POST['simpan']))
{
    include('koneksi.php');
    $lokasi_file = $_FILES['gambar']['tmp_name'];
    $tipe_file   = $_FILES['gambar']['type'];
    $nama_file   = $_FILES['gambar']['name'];
    $direktori   = "../images/gambar_restoran/$nama_file";

    $namanya = $_POST['nama'];

    if ($nama_file == "") {
    	$a =  mysqli_query($koneksi, "UPDATE restoran SET nama_restoran='$_POST[nama]', lokasi='$_POST[lokasi]', waktu='$_POST[waktu]', instagram='$_POST[instagram]', telepon='$_POST[tlp]', maps='$_POST[maps]' WHERE id_restoran='$_GET[id]' ");
        if ($a) {
             echo "<script>
                swal('Berhasil!', 'Data restoran berhasil diubah', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=restoran'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Restoran Gagal Ditambahkan!', 'error')
              </script>";
        }
    }else{
    move_uploaded_file($lokasi_file,$direktori);
    $a =  mysqli_query($koneksi, "UPDATE restoran SET nama_restoran='$_POST[nama]',gambar_restoran='$nama_file' WHERE id_restoran='$_GET[id]' ");
        if ($a) {
             echo "<script>
                swal('Berhasil!', 'Data restoran berhasil diubah', 'success')
            </script>";
            echo "<meta http-equiv='refresh' content='1; url=index.php?halaman=restoran'>";
        }else{
             echo "<script>
                swal('Gagal!', 'Proses gagal', 'error')
              </script>";
        }
    }
} ?>

<script>
    function tampilkanPreview(gambar,idpreview){
//                membuat objek gambar
        var gb = gambar.files;
        
//                loop untuk merender gambar
        for (var i = 0; i < gb.length; i++){
//                    bikin variabel
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview=document.getElementById(idpreview);            
            var reader = new FileReader();
            
            if (gbPreview.type.match(imageType)) {
//                        jika tipe data sesuai
                preview.file = gbPreview;
                reader.onload = (function(element) { 
                    return function(e) { 
                        element.src = e.target.result; 
                    }; 
                })(preview);
                document.getElementById("preview").style.border = "1px solid black";

//                    membaca data URL gambar
                reader.readAsDataURL(gbPreview);
            }else{
//                        jika tipe data tidak sesuai
                alert("Type file tidak sesuai. Khusus image.");
            }
           
        }    
    }
</script>