<?php require_once('config.php'); require_once('fungsi.php');?>
<!DOCTYPE html>
<html lang="en">

<head>

	<?php if(install()==false) { ?>
	
	<!-- Title -->
	<title><?php echo $appdata['title'];?></title>
	
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="description" content="<?php echo $meta['description'];?>">
	<meta name="author" content="<?php echo $meta['author'];?>">
	<meta name="theme-color" content="<?php echo $meta['theme_color'];?>">
	
	<?php if($meta['allow_origin']==true) { header("Access-Control-Allow-Origin: *"); echo '<!-- CORS Is Enabled -->';} ?>
	
	<?php } else { ?>
	
	<!-- Title -->
	<title></title>
	
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	
	<?php }; ?>
	
	<link rel="shortcut icon" href="assets/img/logo-icon-dark.png">
	
	<!-- Vendor CSS -->
	<link href="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/animate.css/animate.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/material-design-iconic-font/css/material-design-iconic-font.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/summernote/dist/summernote.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/chosen/chosen.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/chosen/chosen.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/bootgrid/jquery.botgrid.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
	
	<!-- Main CSS -->
	<link href="<?php echo base_url();?>assets/css/app_1.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/app_2.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/app_ext.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/bootstrap/bootstrap.min.js"></script>
	
</head>

<body>
	
	
	